(ns nubank.player-test
  (:require [clojure.test :refer :all]
            [integrant.core :as ig]
            [nubank.test-utils :refer [with-system]]))

(deftest input-output
  (doseq [set ["1" "2"]
          :let [config {[::input :nubank.io/resource]
                        (str "nubank/data/" set ".input.json")
                        [::output :nubank.io/resource]
                        (str "nubank/data/" set ".output.json")
                        :nubank/player {:queue (ig/ref :nubank/queue)
                                        :input (ig/ref [::input :nubank.io/json-decode])}
                        :nubank/queue {}
                        [::input :nubank.io/json-decode] (ig/ref [::input :nubank.io/resource])
                        [::output :nubank.io/json-decode] (ig/ref [::output :nubank.io/resource])}]]
    (with-system [system config]
      (is (= (get system [::output :nubank.io/json-decode])
             (get system :nubank/player))))))


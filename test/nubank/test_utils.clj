(ns nubank.test-utils
  (:require [integrant.core :as ig]))

(defmacro with-system [[binding config] & body]
  `(let [~binding (-> ~config
                      (doto (ig/load-namespaces))
                      (ig/init))]
     (try
       ~@body
       (finally
         (ig/halt! ~binding)))))

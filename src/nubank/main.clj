(ns nubank.main
  (:gen-class)
  (:require [clojure.java.io :as io]
            [duct.core :as duct]))

(duct/load-hierarchy)

(defn -main [& args]
  (let [keys (or (duct/parse-keys args) [:nubank.io/stdout])]
    (-> (duct/read-config (io/resource "nubank/config.edn"))
        (duct/prep keys)
        (duct/exec keys))))

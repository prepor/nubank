(ns nubank.utils
  (:require [clojure.core.rrb-vector :as fv])
  (:refer-clojure :exclude [find]))

(defn remove-nth [v i]
  (fv/catvec (fv/subvec v 0 i) (fv/subvec v (inc i))))

(defn find [f coll]
  (first (keep f coll)))

(defn indexed-find [f coll]
  (first (keep f (map-indexed vector coll))))

(defmacro cond-let
  "Combination of cond and when-let

  (let [req {:profile \"foo\"}]
    (cond-let
      [profile (:profile req)] (prn \"Porfile!\" profile)
      [user (:user req) (prn \"User!\" user)]))"
  [& clauses]
  (when clauses
    `(if-let ~(first clauses)
       ~(if (next clauses)
          (second clauses)
          (throw (IllegalArgumentException.
                  "cond-let requires an even number of forms")))
       ~(cons `cond-let (next (next clauses))))))

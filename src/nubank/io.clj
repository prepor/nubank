(ns nubank.io
  (:require [clojure.java.io :as io]
            [integrant.core :as ig]
            [cheshire.core :as json]))

(defmethod ig/init-key :nubank.io/resource [_ path]
  (slurp (io/resource path)))

(defmethod ig/init-key :nubank.io/stdin [_ _]
  (slurp *in*))

(defmethod ig/init-key :nubank.io/json-decode [_ s]
  (json/parse-string s true))

(defmethod ig/init-key :nubank.io/json-encode [_ data]
  (json/generate-string data {:pretty true}))

(defmethod ig/init-key :nubank.io/stdout [_ s]
  (print s))



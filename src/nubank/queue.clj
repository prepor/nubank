(ns nubank.queue
  (:require [nubank.utils :as utils]
            [integrant.core :as ig]))

(defprotocol Queue
  (enqueue [this job])
  (dequeue [this agent]))

(defn match-primary? [agent job]
  (contains? (:primary_skillset agent) (:type job)))

(defn match-secondary? [agent job]
  (contains? (:secondary_skillset agent) (:type job)))

(defn match-queue [agent matcher [queue-i queue]]
  (utils/indexed-find (fn [[i job]] (when (matcher agent job) [queue-i i])) queue))

(def matchers [match-primary? match-secondary?])

(defn match* [queues agent matcher]
  (utils/indexed-find (partial match-queue agent matcher) queues))

(defn match [queues agent]
  (utils/find (partial match* queues agent) matchers))

(defn dequeue* [queues result agent]
  (if-let [[queue i] (match queues agent)]
    (do (reset! result (get-in queues [queue i]))
        (update queues queue utils/remove-nth i))
    (reset! result nil)))

(defn make-queue []
  (let [queue (atom [[] []])]
    ;; it's important here to execute actual dequeue inside swap! cycle to
    ;; achieve thread safety
    (reify Queue
      (enqueue [_ job]
        (let [priority (if (:urgent job) 0 1)]
          (swap! queue update priority conj job)))
      (dequeue [_ agent]
        (let [result (atom nil)]
          (swap! queue dequeue* result agent)
          @result)))))

(defmethod ig/init-key :nubank/queue [_ _]
  (make-queue))

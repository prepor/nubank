(ns nubank.player
  (:require [nubank.queue :as queue]
            [nubank.utils :as utils]
            [integrant.core :as ig]
            [clojure.spec.alpha :as s]
            [taoensso.timbre :refer [warn]]))

(defn play-event [agents queue event]
  (utils/cond-let
   [agent (:new_agent event)]
   (let [agent' (-> agent
                    (update :primary_skillset set)
                    (update :secondary_skillset set))]
     (swap! agents assoc (:id agent) agent')
     nil)

   [job (:new_job event)]
   (do (queue/enqueue queue job)
       nil)

   [{:keys [agent_id]} (:job_request event)]
   (if-let [agent (get @agents agent_id)]
     (when-let [{:keys [id]} (queue/dequeue queue agent)]
       [{:job_assigned {:job_id id
                        :agent_id agent_id}}])
     (do (warn "Unknown agent" {:id agent_id})
         nil))
   [_ :else]
   (warn "Unknown event" event)))

(s/def ::id string?)
(s/def ::agent_id ::id)
(s/def ::skill string?)
(s/def ::primary_skillset (s/coll-of ::skill))
(s/def ::secondary_skillset (s/coll-of ::skill))
(s/def ::type string?)
(s/def ::urgetn boolean?)

(s/def ::new_agent (s/keys :req-un [::id
                                    ::name
                                    ::primary_skillset
                                    ::secondary_skillset]))
(s/def ::new_job (s/keys :req-un [::id ::type ::urgent]))
(s/def ::job_request (s/keys :req-un [::agent_id]))

(s/def ::event (s/or :new_agent (s/keys :req-un [::new_agent])
                     :new_job (s/keys :req-un [::new_job])
                     :job_request (s/keys :req-un [::job_request])))

(s/def ::input (s/coll-of ::event))

(defn play [queue input]
  (if (s/valid? ::input input)
    (let [agents (atom {})]
      (loop [[event & events] input output []]
        (if event
          (recur events (apply conj output (play-event agents queue event)))
          output)))
    (throw (ex-info "Invalid input" {:explain (s/explain-str ::input input)}))))

(defmethod ig/init-key :nubank/player [_ {:keys [queue input]}]
  (play queue input))

## Run program

```cat test/nubank/data/1.input.json | docker-compose run nubank```

## Testing

To run tests:

```docker-compose run nubank lein test```

## Design notes

### Queue datastracture

We need to have an ability to efficiently remove an arbitrary element from an
ordered sequence.

Clojure's vector and PersistentQueue are not the best data structures for such
task. That's why we use [RRB-Trees](https://github.com/clojure/core.rrb-vector)

### Thread safety

It wasn't an original requirement, but I assumed that it could be a good feature
:) Concurrent access to job queues is a tricky question, but if we don't have a
lot of parallel requests my solution is simple and efficient.
